package com.util;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.PriorityBlockingQueue;

import org.junit.Before;
import org.junit.Test;

import com.domain.Call;


public class CallComparatorTest {

	CallComparator callComparator;
	PriorityBlockingQueue<Call> calls;
	
	@Before
	public void setup(){
		callComparator = new CallComparator();
		calls = new PriorityBlockingQueue<>(5, callComparator);
	}
	
	@Test
	public void testCallOrderCompare() throws InterruptedException {
		
		Call call1 = new Call(3);
		Call call2 = new Call(4);
		Call call3 = new Call(5);
		Call call4 = new Call(2);
		Call call5 = new Call(1);
		
		calls.add(call1);
		calls.add(call2);
		calls.add(call3);
		calls.add(call4);
		calls.add(call5);
		
		assertEquals(call5, calls.take());
		assertEquals(call4, calls.take());
		assertEquals(call1, calls.take());
		assertEquals(call2, calls.take());
		assertEquals(call3, calls.take());
	}
	
}
