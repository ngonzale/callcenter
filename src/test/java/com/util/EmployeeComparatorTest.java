package com.util;

import java.util.concurrent.PriorityBlockingQueue;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.domain.Employee;

public class EmployeeComparatorTest {


	private PriorityBlockingQueue<Employee> employeeQueue;
	
	
	@Before
	public void setup() {
		employeeQueue = new PriorityBlockingQueue<>(10,new EmployeeComparator());
	}
	
	@Test
	public void testEmployeeTypeComparison() throws InterruptedException {
		
		Employee employee1 = new Employee("operator", Employee.EmployeeType.OPERATOR);
		Employee employee2 = new Employee("supervisor", Employee.EmployeeType.SUPERVISOR);
		Employee employee3 = new Employee("director", Employee.EmployeeType.DIRECTOR);
		
		employeeQueue.add(employee2);
		employeeQueue.add(employee1);
		employeeQueue.add(employee3);
		
		assertEquals(employee1, employeeQueue.take());
		assertEquals(employee2, employeeQueue.take());
		assertEquals(employee3, employeeQueue.take());
		
	}
	
	@Test
	public void testEmployeeLastCallDurationComparison() throws InterruptedException {
		
		Employee employee1 = new Employee("operator1", Employee.EmployeeType.OPERATOR);
		Employee employee2 = new Employee("operator2", Employee.EmployeeType.OPERATOR);
		Employee employee3 = new Employee("operator3", Employee.EmployeeType.OPERATOR);
		
		employee3.setLastPhoneCallDate(1L);
		employee2.setLastPhoneCallDate(2L);
		employee1.setLastPhoneCallDate(3L);
		
		employeeQueue.add(employee2);
		employeeQueue.add(employee1);
		employeeQueue.add(employee3);
		
		assertEquals(employee3, employeeQueue.take());
		assertEquals(employee2, employeeQueue.take());
		assertEquals(employee1, employeeQueue.take());
	}
	
	@Test
	public void testEmployeeLastCallDurationComparisonWithNullValues() throws InterruptedException {
		
		Employee employee1 = new Employee("operator1", Employee.EmployeeType.OPERATOR);
		Employee employee2 = new Employee("operator2", Employee.EmployeeType.OPERATOR);
		Employee employee3 = new Employee("operator3", Employee.EmployeeType.OPERATOR);
		
		employee1.setLastPhoneCallDate(null);
		employee2.setLastPhoneCallDate(1L);
		employee3.setLastPhoneCallDate(null);
		
		employeeQueue.add(employee1);
		employeeQueue.add(employee2);
		employeeQueue.add(employee3);
		
		assertEquals(employee1, employeeQueue.take());
		assertEquals(employee3, employeeQueue.take());
		assertEquals(employee2, employeeQueue.take());
	}
	
	
	
	@Test
	public void testEmployeeNameComparison() throws InterruptedException {
		
		Employee employee1 = new Employee("A", Employee.EmployeeType.OPERATOR);
		Employee employee2 = new Employee("B", Employee.EmployeeType.OPERATOR);
		Employee employee3 = new Employee("C", Employee.EmployeeType.OPERATOR);
		
		employee3.setLastPhoneCallDate(1L);
		employee2.setLastPhoneCallDate(1L);
		employee1.setLastPhoneCallDate(1L);
		
		employeeQueue.add(employee2);
		employeeQueue.add(employee1);
		employeeQueue.add(employee3);
		
		assertEquals(employee1, employeeQueue.take());
		assertEquals(employee2, employeeQueue.take());
		assertEquals(employee3, employeeQueue.take());
	}


}
