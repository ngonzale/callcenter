package com.service;

import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.concurrent.PriorityBlockingQueue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.domain.AnswerMachine;
import com.domain.Call;
import com.domain.Employee;
import com.domain.Employee.EmployeeType;
import com.util.CallComparator;
import com.util.EmployeeComparator;

public class DispatcherTest {

	// SUT
	private Dispatcher dispatcher;
	private PriorityBlockingQueue<Employee> employees;
	private PriorityBlockingQueue<Call> calls;
	private AnswerMachine answerMachine;

	@Before
	public void setup() {

		this.calls = new PriorityBlockingQueue<>(10, new CallComparator());
		this.employees = new PriorityBlockingQueue<>(10, new EmployeeComparator());
		this.answerMachine = new AnswerMachine(calls);
	}

	@Test
	public void testEmployeeHandlingCall() throws InterruptedException {

		Employee operator = Mockito.mock(Employee.class);
		employees.add(operator);

		Call singleCall = new Call(1);
		calls.add(singleCall);

		PriorityBlockingQueue<Call> spiedCalls = Mockito.spy(calls);
		Mockito.when(spiedCalls.take()).thenReturn(singleCall).thenReturn(null);

		//SUT
		this.dispatcher = new Dispatcher(employees, spiedCalls, answerMachine);
		dispatcher.run();

		Mockito.verify(operator).pickUpNextCall(Mockito.eq(singleCall), Mockito.any(CallHandlerWrapper.class));
	}

	@Test
	public void testThreeEmployeesHandlingCalls() throws InterruptedException {

		/* Build employees */
		Employee operator = new Employee("Operator1", EmployeeType.OPERATOR);
		Employee operator2 = new Employee("Operator2", EmployeeType.OPERATOR);
		Employee director = new Employee("director", EmployeeType.DIRECTOR);
		Employee supervisor = new Employee("supervisor", EmployeeType.SUPERVISOR);

		/*
		 * Build related spy objects. Cannot be mocked (with Mockito.mock) cause
		 * their names and types would'n be able to be tested
		 */
		Employee spiedOperator = Mockito.spy(operator);
		Employee spiedOperator2 = Mockito.spy(operator2);
		Employee spiedDirector = Mockito.spy(director);
		Employee spiedSupervisor = Mockito.spy(supervisor);

		employees.add(spiedOperator);
		employees.add(spiedOperator2);
		employees.add(spiedDirector);
		employees.add(spiedSupervisor);

		Call singleCall1 = new Call(1);
		Call singleCall2 = new Call(2);
		Call singleCall3 = new Call(3);

		calls.add(singleCall1);
		calls.add(singleCall2);
		calls.add(singleCall3);

		PriorityBlockingQueue<Call> spiedCalls = Mockito.spy(calls);

		// Avoid dispatcher to still waiting reentrant calls. Just take the
		// first three in the order specified below.
		Mockito.when(spiedCalls.take()).thenReturn(singleCall1).thenReturn(singleCall2).thenReturn(singleCall3)
				.thenReturn(null);

		//SUT
		this.dispatcher = new Dispatcher(employees, spiedCalls, answerMachine);
		dispatcher.run();

		/*
		 * Test given role priority behavior in which a Director has the
		 * lowerone
		 */
		Mockito.verify(spiedOperator).pickUpNextCall(Mockito.eq(singleCall1), Mockito.any(CallHandlerWrapper.class));
		Mockito.verify(spiedOperator2).pickUpNextCall(Mockito.eq(singleCall2), Mockito.any(CallHandlerWrapper.class));
		Mockito.verify(spiedSupervisor).pickUpNextCall(Mockito.eq(singleCall3), Mockito.any(CallHandlerWrapper.class));
		Mockito.verify(spiedDirector, Mockito.never()).pickUpNextCall(Mockito.any(Call.class),
				Mockito.any(CallHandlerWrapper.class));
	}

	/**
	 * This test show how AnwerMachine comes on the scene. When all the
	 * employees {@link EmployeeType.OPERATOR}, {@link EmployeeType.DIRECTOR or
	 * {@link EmployeeType.SUPERVISOR} are busy, an instance of
	 * {@link AnswerMachine} start to handle calls, waiting for any Employee (no
	 * matter it type) to finish their work. Note: As exercise statement don't
	 * specify the above behavior, this is an hypothesis: If all employees are
	 * busy within a call, when a new a call enters the pool to be handled, the
	 * first employee who ends with his call handle that pending call (NO MATTER
	 * if he'is a operator, supervisor or director). The priority in this
	 * context is to take the call as soon as possible, without waiting to less
	 * busy employee
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void testAnserMachineHandlingCallWhenNoEmployeeAvailable() throws InterruptedException {

		Call singleCall1 = Mockito.mock(Call.class);
		calls.add(singleCall1);
		PriorityBlockingQueue<Call> spiedCalls = Mockito.spy(calls);

		AnswerMachine answerMachine = new AnswerMachine(spiedCalls);
		AnswerMachine spiedAnswerMachine = Mockito.spy(answerMachine);

		/*
		 * Clear is no needed, but it is useful to highlight the main goal of
		 * the test
		 */
		employees.clear();

		// Avoid dispatcher to still waiting reentrant calls. Just take the
		// first one.
		Mockito.when(spiedCalls.take()).thenReturn(singleCall1).thenReturn(null);

		//SUT
		this.dispatcher = new Dispatcher(employees, spiedCalls, spiedAnswerMachine);
		this.dispatcher.run();

		verify(spiedAnswerMachine).pickUpNextCall(Mockito.eq(singleCall1), Mockito.any(CallHandlerWrapper.class));
	}

	@Test
	public void testHandlingTenCallAtTheSameTime() throws InterruptedException {

		//Build employees, no need to mock them
		Employee operator1 = new Employee("Operator1", EmployeeType.OPERATOR);
		Employee operator2 = new Employee("Operator2", EmployeeType.OPERATOR);
		Employee operator3 = new Employee("Operator3", EmployeeType.OPERATOR);
		Employee operator4 = new Employee("Operator4", EmployeeType.OPERATOR);
		Employee operator5 = new Employee("Operator5", EmployeeType.OPERATOR);
		Employee operator6 = new Employee("Operator6", EmployeeType.OPERATOR);
		Employee supervisor1 = new Employee("supervisor1", EmployeeType.SUPERVISOR);
		Employee supervisor2 = new Employee("supervisor2", EmployeeType.SUPERVISOR);
		Employee supervisor3 = new Employee("supervisor3", EmployeeType.SUPERVISOR);
		Employee director1 = new Employee("director1", EmployeeType.DIRECTOR);

		employees.addAll(Arrays.asList(operator1, operator2, operator3, operator4, operator5, operator6, supervisor1,
				supervisor2, supervisor3, director1));

		//Build calls, no need to mock them
		Call call1 = new Call(1);
		Call call2 = new Call(2);
		Call call3 = new Call(3);
		Call call4 = new Call(4);
		Call call5 = new Call(5);
		Call call6 = new Call(6);
		Call call7 = new Call(7);
		Call call8 = new Call(8);
		Call call9 = new Call(9);
		Call call10 = new Call(10);

		calls.addAll(Arrays.asList(call1, call2, call3, call4, call5, call6, call7, call8, call9, call10));

		PriorityBlockingQueue<Call> spiedCalls = Mockito.spy(calls);
		AnswerMachine answerMachine = new AnswerMachine(spiedCalls);
		
		AnswerMachine spiedAnswerMachine = Mockito.spy(answerMachine);

		Mockito.when(spiedCalls.take()).thenReturn(call1).
									    thenReturn(call2).
									    thenReturn(call3).
									    thenReturn(call4).
									    thenReturn(call5).
									    thenReturn(call6).
									    thenReturn(call7).
									    thenReturn(call8).
									    thenReturn(call9).
									    thenReturn(call10).
									    thenReturn(null);

		//SUT
		this.dispatcher = new Dispatcher(employees, spiedCalls, spiedAnswerMachine);
		this.dispatcher.run();

		//If Answer machine didn't do anything, means that all the 10 calls could be handled for the 10 employees at the same time without any problem
		verify(spiedAnswerMachine, Mockito.never()).pickUpNextCall(Mockito.any(Call.class), Mockito.any(CallHandlerWrapper.class));
	}
}
