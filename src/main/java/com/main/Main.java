package com.main;

import java.lang.reflect.Array;
import java.util.concurrent.PriorityBlockingQueue;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import com.domain.AnswerMachine;
import com.domain.Call;
import com.domain.Employee;
import com.domain.Employee.EmployeeType;
import com.service.CallCenter;
import com.util.CallComparator;
import com.util.EmployeeComparator;

/**
 * Main
 * 
 * @author nicolas.gonzalez
 */
public class Main {

	private static Integer operatorsNumber;
	private static Integer supervisorsNumber;
	private static Integer directorsNumber;
	private static Integer callsNumber;;

	/**
	 * Main class
	 * 
	 * @param args {@link String} array
	 * @throws InterruptedException InterruptedException
	 */
	public static void main(final String[] args) {

		try {
			setArgumentsValues(args);

			final PriorityBlockingQueue<Employee> employees = new PriorityBlockingQueue<>(20, new EmployeeComparator());
			final PriorityBlockingQueue<Call> calls = new PriorityBlockingQueue<Call>(1000, new CallComparator());
			final AnswerMachine anserMachine = new AnswerMachine(calls);

			buildEmployee(EmployeeType.OPERATOR, employees, operatorsNumber);
			buildEmployee(EmployeeType.SUPERVISOR, employees, supervisorsNumber);
			buildEmployee(EmployeeType.DIRECTOR, employees, directorsNumber);

			final CallCenter callCenter = new CallCenter(employees, calls, anserMachine);

			generateCalls(callCenter, callsNumber);

			callCenter.startDispatcher();
		
		} catch (InterruptedException e) {
			System.out.println("Unepected error happended: " + e.getMessage());
		} catch (ParseException e) {
			System.out.println("Unepected error happended: " + e.getMessage());
		}
	}

	/**
	 * Set command line arguments
	 * @param args {@link Array} of {@link String}
	 * @throws ParseException when missing or invalid arguments
	 */
	private static void setArgumentsValues(final String[] args) throws ParseException {

		CommandLine commandLine;

		Option operatorsOption = Option.builder("o").argName("Operators Number").hasArg().type(Number.class)
				.desc("The number of operators").required(true).build();
		Option supervisorOption = Option.builder("s").argName("Supervisor Number").hasArg().desc("The number of supervisors")
				.required(true).build();
		Option directorOption = Option.builder("d").argName("Directors Number").hasArg().desc("The number of directors")
				.required(true).build();
		Option callsOption = Option.builder("c").argName("Calls Number").hasArg().desc("The number of calls")
				.required(true).build();

		Options options = new Options();
		CommandLineParser parser = new DefaultParser();

		HelpFormatter formatter = new HelpFormatter();
		String header = "\n -o <number of operators> \n -s <number of supervisors> \n -d <number of directors> \n -c <number of calls> \n  (Arguments may be in any order)";
		String footer = "by nicolas.hernan.gonzalez@gmail.com";
		formatter.printHelp("java -jar CallCenter.jar", header, options, footer, true);
		System.out.println();

		options.addOption(operatorsOption);
		options.addOption(supervisorOption);
		options.addOption(directorOption);
		options.addOption(callsOption);

		commandLine = parser.parse(options, args);

		try {
			operatorsNumber = Integer.valueOf(commandLine.getOptionValue("o"));
			supervisorsNumber = Integer.valueOf(commandLine.getOptionValue("s"));
			directorsNumber = Integer.valueOf(commandLine.getOptionValue("d"));
			callsNumber = Integer.valueOf(commandLine.getOptionValue("c"));
		} catch (NumberFormatException e) {
			throw new ParseException("Invalid format number"); 
		}
	}

	/**
	 * Generate Mocking calls
	 * 
	 * @param callCenter {@link CallCenter}
	 * @param qty calls quantity for mock
	 */
	private static void generateCalls(final CallCenter callCenter, final int qty) {

		for (int i = 0; i < qty; i++) {
			callCenter.addCall(new Call(i));
		}
	}

	/**
	 * Create Mocking employees
	 * 
	 * @param type {@link EmployeeType}
	 * @param employees {@link PriorityBlockingQueue}
	 * @param qty employee quantity for mock
	 */
	private static void buildEmployee(final EmployeeType type, final PriorityBlockingQueue<Employee> employees,
			final int qty) {

		for (int i = 0; i < qty; i++) {
			employees.add(new Employee(type + " " + String.valueOf(i), type));
		}
	}
}
