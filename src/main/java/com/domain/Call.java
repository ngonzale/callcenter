package com.domain;

import java.util.concurrent.ThreadLocalRandom;

import com.service.CallCenter;

/**
 * Abstraction of simple call need to be attend for a {@link CallCenter}
 * {@link Employee}
 * (Not declared as final class for mockito testing purpose)
 * @author nicolas.gonzalez
 */
public class Call {
	private static final int SINCE_DURATION = 5000;
	private static final int TO_DURATION = 10000;
	private final int number;
	private final int duration;

	

	/**
	 * @param callNumber number of the {@link Call}
	 */
	public Call(final int callNumber) {
		super();
		number = callNumber;
		duration = ThreadLocalRandom.current().nextInt(SINCE_DURATION, TO_DURATION);
	}

	/**
	 * getter
	 *
	 * @return call order
	 */
	public int getCallNumber() {
		return number;
	}

	/**
	 * getter
	 *
	 * @return duration
	 */
	public int getDuration() {
		return duration;
	}

	/**
	 * method that simulate a call being picked up
	 *
	 * @throws InterruptedException {@link InterruptedException}
	 */
	public void pickup() throws InterruptedException {
		Thread.sleep(duration);
	}

	@Override
	public String toString() {
		return "Call [callNumber=" + number + ", duration=" + duration + "]";
	}
}
