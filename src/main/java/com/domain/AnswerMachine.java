package com.domain;

import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.domain.Employee.EmployeeType;
import com.service.CallHandlerWrapper;
import com.service.EmployeeQueueListener;

/**
 * Representation of an Answer machine which starts to run when there are no
 * available employees for call answering.
 * (Not declared as final class for mockito testing purpose)
 *
 * @author nicolas.gonzalez
 */
public class AnswerMachine implements CallHandler, EmployeeQueueListener {

//	final static Logger logger = Logger.getLogger("sa");
	private final BlockingQueue<Call> internalCallsPool; 
	private final Queue<Call> realCallsPool;
	
	/**
	 * 
	 * @param calls {@link Queue} pool reference
	 */
	public AnswerMachine(final Queue<Call> calls) {
		this.internalCallsPool = new LinkedBlockingQueue<>();
		this.realCallsPool = calls;	
	}
	
	 
	@Override
	public void pickUpNextCall(final Call call, final CallHandlerWrapper callHandlerWrapper)
			throws InterruptedException {

		System.out.println("Answer Machine holding on call number: " + call.getCallNumber());
		
		//Add the call into internal pool, waiting for an available employee (maybe listening to wait message :D)
		this.internalCallsPool.put(call);
	}
	

	@Override
	public void employeeAvailable() {
		try {
			 	//Maybe there are no call no take.
				if (!this.internalCallsPool.isEmpty()) {
			 		this.realCallsPool.add(this.internalCallsPool.take());
			 		System.out.println("AnswerMachine releasing call...");
			 	}
		
		} catch (InterruptedException e) {
			System.out.println("Unepected error happended: " + e.getMessage());
		}
	}
	
	/**
	 * getter
	 */
	@Override
	public String getName() {
		return "Answer Machine";
	}
	
	/**
	 * getter
	 */
	@Override
	public EmployeeType getType() {
		return null;
	}
}
