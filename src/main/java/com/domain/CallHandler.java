package com.domain;

import java.util.concurrent.BlockingQueue;

import com.domain.Employee.EmployeeType;
import com.service.CallHandlerWrapper;

/**
 * Call handler aware
 *
 * @author nicolas.gonzalez
 */
public interface CallHandler {
	/**
	 * Take a call from the {@link BlockingQueue} to be handled by
	 * {@link Employee} setting its last update call parameter. Define a
	 * callback function where the employee return to the queue once the call
	 * ends.
	 *
	 * @param call {@link Call} call  to be answered
	 * @param callHandlerWrapper {@link CallHandlerWrapper}
	 * @throws InterruptedException {@link InterruptedException}
	 */
	void pickUpNextCall(Call call, CallHandlerWrapper callHandlerWrapper) throws InterruptedException;

	/**
	 * Getter
	 *
	 * @return {@link String} name of handler
	 */
	String getName();

	/**
	 * Getter
	 *
	 * @return {@link EmployeeType} employee type
	 */
	EmployeeType getType();
}
