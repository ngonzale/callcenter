package com.domain;

import java.util.concurrent.CompletableFuture;

import com.service.CallHandlerWrapper;

/**
 * Representation of an Employee of the {@link CallHandler} whose main
 * responsibility is pick up calls
 * (Not declared as final class for mockito testing purpose)
 * @author nicolas.gonzalez
 */
public class Employee implements CallHandler {
	/**
	 * Represent an {@link Employee} type, an ordered by importance
	 *
	 * @author nicolas.gonzalez
	 */
	public enum EmployeeType {
		OPERATOR, SUPERVISOR, DIRECTOR;
	}

	private final String name;
	private final EmployeeType type;
	private Long lastPhoneCallDate;

	/**
	 * Constructor
	 *
	 * @param employeeName {@link String} Employee name
	 * @param employeeType {@link String} Employee {@link EmployeeType}
	 */
	public Employee(final String employeeName, final EmployeeType employeeType) {
		super();
		name = employeeName;
		type = employeeType;
	}

	@Override
	public void pickUpNextCall(final Call call, final CallHandlerWrapper callHandlerWrapper) throws InterruptedException {
		
		System.out.println("Employee : " + getName() + " taking the call " + call.getCallNumber());
		
		CompletableFuture.runAsync(() -> takeCall(call)).whenComplete((voidValue, error) -> {
			
			this.setLastPhoneCallDate(System.currentTimeMillis());
			
			callHandlerWrapper.employeeAvailableAgain(this);
			
			System.out.println("Employee : " + getName() + " available again");
		});
	}

	/**
	 * @param call {@link Call} to be picked up
	 */
	private void takeCall(final Call call) {
		try {
			call.pickup();
		} catch (final InterruptedException e) {
			System.out.println("Unepected error happended: " + e.getMessage());
		}
	}

	@Override
	public EmployeeType getType() {
		return type;
	}

	/**
	 * Getter
	 *
	 * @return {@link Long} last assigned {@link Call} duration in milliseconds
	 */
	public Long getLastPhoneCallDate() {
		return lastPhoneCallDate;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + "]";
	}

	/**
	 * @param date {@link Long} representing last call duration in milliseconds
	 */
	public void setLastPhoneCallDate(final Long date) {
		lastPhoneCallDate = date;
	}
}
