package com.service;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

import com.domain.AnswerMachine;
import com.domain.Call;
import com.domain.CallHandler;
import com.domain.Employee;

/**
 * Class responsible for assign each available employee a call due its profile
 *
 * @author nicolas.gonzalez
 *
 */
public class Dispatcher implements Runnable {

	private final PriorityBlockingQueue<Call> callsToBePickedUp;
	private final CallHandlerWrapper callhandlerWrapper;
	
	/**
	 * Constructor
	 *
	 * @param employees
	 *            {@link PriorityBlockingQueue} of {@link Employee} set up into
	 *            the {@link CallCenter}
	 *
	 * @param calls
	 *            {@link BlockingQueue} of {@link CallCenter} Calls to be picked
	 *            up
	 *          
	 * @param answerMachine {@link CallHandler} to pick up {@link Call} when no {@link Employee} available
	 */
	public Dispatcher(final PriorityBlockingQueue<Employee> employees, final PriorityBlockingQueue<Call> calls, final AnswerMachine answerMachine) {
		this.callsToBePickedUp = calls;
		/*mmm maybe would be better change this relation*/
		this.callhandlerWrapper = new CallHandlerWrapper(employees, answerMachine);
	}

	@Override
	public void run() {
		
		Call call = null;
		
		CallHandler callHandler = null;
		
		try {
			while ((call = callsToBePickedUp.take()) != null) {

				callHandler = callhandlerWrapper.getAvailableEmployee();

				callHandler.pickUpNextCall(call, callhandlerWrapper);
			}
		} catch (final InterruptedException e) {
			System.out.println("Unepected error happended: " + e.getMessage());
		}
	}
}
