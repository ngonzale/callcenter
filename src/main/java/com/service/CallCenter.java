package com.service;

import java.util.Collection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

import com.domain.AnswerMachine;
import com.domain.Call;
import com.domain.Employee;

/**
 * Representation of a call center which allows add calls and is responsible for
 * start calls dispatching
 *
 * @author nicolas.gonzalez
 *
 */
public final class CallCenter {

	private final PriorityBlockingQueue<Call> callCenterCalls;
	private final Dispatcher dispatcher;
	private final PriorityBlockingQueue<Employee> callCenterEmployees;
	private final AnswerMachine callCenterAnswerMachine;

	/**
	 *
	 * @param employees {@link PriorityBlockingQueue} set up employees for this {@link CallCenter}
	 * @param calls {@link PriorityBlockingQueue} with calls to be picked up
	 * @param answerMachine {@link CallCenter} also must be configured with an {@link AnswerMachine} who handle call when no employees available.    
	 */
	public CallCenter(final PriorityBlockingQueue<Employee> employees, final PriorityBlockingQueue<Call> calls, final AnswerMachine answerMachine) {
		super();
		this.callCenterCalls = calls;
		this.callCenterEmployees = employees;
		this.callCenterAnswerMachine = answerMachine;
		
		dispatcher = new Dispatcher(callCenterEmployees, callCenterCalls, callCenterAnswerMachine);	
	}

	/**
	 * add call for pending call {@link BlockingQueue}
	 *
	 * @param call {@link Call}
	 */
	public void addCall(final Call call) {
		callCenterCalls.add(call);
	}
	
	/**
	 * add call for pending call {@link BlockingQueue}
	 *
	 * @param calls  {@link Collection} of {@link Call}
	 */
	public void addCalls(final Collection<Call> calls) {
		calls.addAll(calls);
	}

	/**
	 * Starts dispatch calls process
	 *
	 * @throws InterruptedException
	 *             {link InterruptedException}
	 */
	public void startDispatcher() throws InterruptedException {
		new Thread(dispatcher).start();
		/*Eventually, call center could do more stuff than just dispatch calls*/
	}

}
