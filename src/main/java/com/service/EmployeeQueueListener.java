package com.service;

/**
 * @author nicolas.gonzalez 
 * Main goal is communicating whenever an employee is available again in the pool
 */
public interface EmployeeQueueListener {

	/**
	 * Notify when a employee is available again to take calls
	 */
	void employeeAvailable();

}
