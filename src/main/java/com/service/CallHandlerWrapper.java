package com.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;

import com.domain.AnswerMachine;
import com.domain.CallHandler;
import com.domain.Employee;

/**
 * Wrapper of an {@link Employee} {@link Queue}
 *
 * @author nicolas.gonzalez
 */
public final class CallHandlerWrapper {
	
	private final PriorityBlockingQueue<Employee> availableEmployees;
	private List<EmployeeQueueListener> listeners = new ArrayList<>();
	private AnswerMachine myAnswerMachine;
	
	/**
	 * Default constructor
	 *
	 * @param employees {@link PriorityBlockingQueue} call center set up
	 *            employees
	 * @param answerMachine {@link AnswerMachine}           
	 */
	public CallHandlerWrapper(final PriorityBlockingQueue<Employee> employees, final AnswerMachine answerMachine) {
		super();
		availableEmployees = employees;
		this.myAnswerMachine = answerMachine;
		this.registerEmployeeAvailableListener(answerMachine);
	}

	/**
	 * Returns a {@link CallHandler} to handle next call
	 *
	 * @return {@link CallHandler} if {@link PriorityBlockingQueue} is not
	 *         empty,returning an instance of {@link AnswerMachine} otherwise.
	 */
	public CallHandler getAvailableEmployee() {
		if (availableEmployees.isEmpty()) {
			return myAnswerMachine;
		}
		return availableEmployees.poll();
	}

	/**
	 * Mark a {@link Employee} like available gettin back him to the
	 * {@link PriorityBlockingQueue}
	 *
	 * @param employee {@link Employee}
	 */
	public void employeeAvailableAgain(final Employee employee) {
		
		availableEmployees.add(employee);
		
		notifyEmployeeAvailable();
	}
	
	/**
	 * Add the listener to the list of registered listeners
	 * @param listener {@link EmployeeQueueListener}
	 */
	public void registerEmployeeAvailableListener(final EmployeeQueueListener listener) {
        this.listeners.add(listener);
    }
	
	
	/**
	 * 
	 * Notify to the listeners that an employee is available again
	 */
	public synchronized void notifyEmployeeAvailable() { 
		this.listeners.forEach(listener -> listener.employeeAvailable());
    }
}
