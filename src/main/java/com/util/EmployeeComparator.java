package com.util;

import java.util.Comparator;
import com.domain.Employee;

/**
 * compare to {@link Employee} due its Type and their last picked up call
 * date
 */
public class EmployeeComparator implements Comparator<Employee> {
	
	/**
	 * This criteria is an assumption, while is not specified in the exercise statement
	 */
	@Override
	public int compare(final Employee c1, final Employee c2) {
		if (c1.getType().equals(c2.getType())) {
			int compare = getLastAssignedCallComparator(c1, c2);
			if (compare == 0) {
				return alphabeticNameComparator(c1.getName(), c2.getName());
			}
			return compare;
		}
		return c1.getType().ordinal() - c2.getType().ordinal();
	}

	/**
	 * Just for testing purposes
	 * @param c1 first {@link String} name to be compared 
	 * @param c2 second {@link String} name to be compared
 	 * @return {@link Integer}
	 */
	private int alphabeticNameComparator(final String c1, final String c2) {
        return c1.compareTo(c2);
	}

	/**
	 * 
	 * @param c1 first {@link Employee} to compare
	 * @param c2 second {@link Employee} to compare
	 * @return {@link Integer}
	 */
	private int getLastAssignedCallComparator(final Employee c1, final Employee c2) {
		if (c1.getLastPhoneCallDate() == null && c2.getLastPhoneCallDate() == null) {
			return 0;
		}
		//If the date field is null, means that that employee never took a call.
		if (c1.getLastPhoneCallDate() != null && c2.getLastPhoneCallDate() == null) {
			return 1;
		}
		if (c1.getLastPhoneCallDate() == null && c2.getLastPhoneCallDate() != null) {
			return -1;
		}
		return c1.getLastPhoneCallDate().compareTo(c2.getLastPhoneCallDate());
	}
}
