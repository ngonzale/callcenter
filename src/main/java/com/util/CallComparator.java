package com.util;

import java.util.Comparator;

import com.domain.Call;

/**
 * Comparator to sort calls queue by call's number 
 * @author nicolas.gonzalez
 *
 */
public class CallComparator implements Comparator<Call> {

	@Override
	public int compare(final Call c1, final Call c2) {
		return c1.getCallNumber() - c2.getCallNumber();
	}
}
